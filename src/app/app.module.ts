import { BrowserModule } from '@angular/platform-browser';
import { NgModule, OnInit } from '@angular/core';


import { AppComponent } from './app.component';
import { Route1Component } from './route1/route1.component';
import { Route2Component } from './route2/route2.component';
import { HomeComponent } from './home/home.component';
import { RouterModule } from '@angular/router';
import { APP_ROUTES } from './app.routes';
import { APP_BASE_HREF, HashLocationStrategy, Location, LocationStrategy } from '@angular/common';


@NgModule({
  declarations: [
    AppComponent,
    Route1Component,
    Route2Component,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(APP_ROUTES)
  ],
  providers: [
    { provide: APP_BASE_HREF, useValue: '/' },
    { provide: LocationStrategy, useClass: HashLocationStrategy }
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule implements OnInit {
  constructor(private location: Location) {
  }

  ngOnInit(): void {
    this.location.subscribe(
      locationChangeEvent => {
        console.log('change');
        console.log(this.location.path());
        console.log(locationChangeEvent.url);
      }
    );
  }


}
